#ifndef FILESYSTEMEVENTLISTENER_H
#define FILESYSTEMEVENTLISTENER_H

#include <QString>
#include <QByteArray>

class FileSystemEventListener {
public slots:
  virtual void onFileCreated(QString path, QByteArray hash) = 0;
  virtual void onFileUpdated(QString path, QByteArray hash) = 0;
  virtual void onFileDeleted(QString path, QByteArray hash) = 0;
  virtual void onDirectoryCreated(QString path) = 0;
  virtual void onDirectoryDeleted(QString path) = 0;
};

#endif // FILESYSTEMEVENTLISTENER_H
