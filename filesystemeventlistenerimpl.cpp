#include "filesystemeventlistenerimpl.h"
#include "synchronizedscope.h"
#include <QDebug>

FileSystemEventListenerImpl::FileSystemEventListenerImpl(const QString &_listenerName, QObject *parent) : QObject(parent), FileSystemEventListener()
{
    this->mutex = new QMutex();
    this->listenerName = _listenerName;
}

FileSystemEventListenerImpl::~FileSystemEventListenerImpl()
{
    free(this->mutex);
}

void FileSystemEventListenerImpl::onFileCreated(QString path, QByteArray hash) {
    SynchronizedScope synchronized(this->mutex);
    qDebug() << "Listener " << this->listenerName << " :: File created :: " << path << " :: Hash :: " << transformByArrayToUserFriendlyForm(hash);
    if (this->existingFiles.contains(path)) {
        qDebug() << "File already exists T_T";
        //throw(1);
    }
    this->existingFiles.insert(path);
    if (path.endsWith("wahaha.txt")) {
        this->report();
    }
}

void FileSystemEventListenerImpl::onFileUpdated(QString path, QByteArray hash) {
    SynchronizedScope synchronized(this->mutex);
    qDebug() << "Listener " << this->listenerName << " :: File updated :: " << path << " :: Hash :: " << transformByArrayToUserFriendlyForm(hash);
    if (!this->existingFiles.contains(path)) {
        qDebug() << "No file T_T";
        //throw(1);
    }
}

void FileSystemEventListenerImpl::onFileDeleted(QString path, QByteArray hash) {
    SynchronizedScope synchronized(this->mutex);
    qDebug() << "Listener " << this->listenerName << " :: File deleted :: " << path << " :: Hash :: " << transformByArrayToUserFriendlyForm(hash);
    if (!this->existingFiles.contains(path)) {
        qDebug() << "No file T_T";
        //throw(1);
    } else {
        this->existingFiles.remove(path);
    }
}

void FileSystemEventListenerImpl::onDirectoryCreated(QString path) {
    SynchronizedScope synchronized(this->mutex);
    qDebug() << "Listener " << this->listenerName << " :: Directory created :: " << path;
    if (this->existingDirs.contains(path)) {
        qDebug() << "Directory already exists T_T";
        //throw(1);
    }
    this->existingDirs.insert(path);
}

void FileSystemEventListenerImpl::onDirectoryDeleted(QString path) {
    SynchronizedScope synchronized(this->mutex);
    qDebug() << "Listener " << this->listenerName << " :: Directory deleted :: " << path;
    if (!this->existingDirs.contains(path)) {
        qDebug() << "No directory T_T";
        //throw(1);
    } else {
        this->existingDirs.remove(path);
    }
}

QString FileSystemEventListenerImpl::transformByArrayToUserFriendlyForm(const QByteArray &arr) {
    return QString(arr.toHex());
}

void FileSystemEventListenerImpl::report() {
    for (QString existingDir : this->existingDirs.toList()) {
        qDebug() << "Existing directory ::: " << existingDir;
    }
    for (QString existingFile : this->existingFiles.toList()) {
        qDebug() << "Existing file ::: " << existingFile;
    }
    qDebug() << "report is done";
}
