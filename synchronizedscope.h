#ifndef SYNCHRONIZEDBLOCK_H
#define SYNCHRONIZEDBLOCK_H

#include <QMutex>

class SynchronizedScope
{
public:
    SynchronizedScope(QMutex *_mutex);
    ~SynchronizedScope();

private:
    QMutex *mutex;
};

#endif // SYNCHRONIZEDBLOCK_H
