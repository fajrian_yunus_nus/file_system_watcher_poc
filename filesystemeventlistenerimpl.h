#ifndef FILESYSTEMEVENTLISTENERIMPL_H
#define FILESYSTEMEVENTLISTENERIMPL_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QMutex>
#include <QSet>
#include "filesystemeventlistener.h"

class FileSystemEventListenerImpl : public QObject, public FileSystemEventListener
{
    Q_OBJECT
public:
    explicit FileSystemEventListenerImpl(const QString &_listenerName, QObject *parent = 0);
    ~FileSystemEventListenerImpl();

signals:

public slots:
    void onFileCreated(QString path, QByteArray hash);
    void onFileUpdated(QString path, QByteArray hash);
    void onFileDeleted(QString path, QByteArray hash);
    void onDirectoryCreated(QString path);
    void onDirectoryDeleted(QString path);

private:
    QString listenerName;
    QMutex *mutex;
    QSet<QString> existingDirs;
    QSet<QString> existingFiles;
    QString transformByArrayToUserFriendlyForm(const QByteArray &arr);
    void report();
};

#endif // FILESYSTEMEVENTLISTENERIMPL_H
