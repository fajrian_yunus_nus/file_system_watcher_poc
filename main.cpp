#include "fullfilesystemwatcher.h"
#include <QApplication>

#include <QFileSystemWatcher>
#include <QDebug>

#include "filesystemeventlistener.h"
#include "filesystemeventlistenerimpl.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (argc < 2) {
        qFatal("Path is not specified. The directory path must be the first parameter.");
        return 1;
    }

    const QString rootPath = argv[1];

    QFileSystemWatcher systemGivenWatcher;

    qDebug() << "User specified path: " << rootPath;

    FileSystemEventListenerImpl myListener("my_listener");

    FullFileSystemWatcher* fullFileSystemWatcher = new FullFileSystemWatcher(rootPath, &systemGivenWatcher);

    QObject::connect(&systemGivenWatcher, SIGNAL(directoryChanged(QString)), fullFileSystemWatcher, SLOT(onDirectoryEvent(QString)));
    QObject::connect(&systemGivenWatcher, SIGNAL(fileChanged(QString)), fullFileSystemWatcher, SLOT(onFileEvent(QString)));

    QObject::connect(fullFileSystemWatcher, SIGNAL(fileCreatedSignal(QString,QByteArray)), &myListener, SLOT(onFileCreated(QString,QByteArray)));
    QObject::connect(fullFileSystemWatcher, SIGNAL(fileUpdatedSignal(QString,QByteArray)), &myListener, SLOT(onFileUpdated(QString,QByteArray)));
    QObject::connect(fullFileSystemWatcher, SIGNAL(fileDeletedSignal(QString,QByteArray)), &myListener, SLOT(onFileDeleted(QString,QByteArray)));
    QObject::connect(fullFileSystemWatcher, SIGNAL(directoryCreatedSignal(QString)), &myListener, SLOT(onDirectoryCreated(QString)));
    QObject::connect(fullFileSystemWatcher, SIGNAL(directoryDeletedSignal(QString)), &myListener, SLOT(onDirectoryDeleted(QString)));


    fullFileSystemWatcher->start();

    return a.exec();
}
