#-------------------------------------------------
#
# Project created by QtCreator 2016-03-07T02:52:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = file_system_watcher_poc
TEMPLATE = app


SOURCES += main.cpp\
    filesystemeventlistenerimpl.cpp \
    synchronizedscope.cpp \
    fullfilesystemwatcher.cpp

HEADERS  += \
    filesystemeventlistener.h \
    filesystemeventlistenerimpl.h \
    synchronizedscope.h \
    fullfilesystemwatcher.h

FORMS    +=
