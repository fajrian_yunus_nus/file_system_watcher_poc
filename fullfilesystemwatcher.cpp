#include "fullfilesystemwatcher.h"
#include <QCryptographicHash>
#include <QDir>
#include <QFileInfoList>
#include <QObject>
#include "synchronizedscope.h"

FullFileSystemWatcher::FullFileSystemWatcher(const QString &rootPath, QFileSystemWatcher *_systemGivenWatcher, QObject *parent) : QObject(parent)
{
    this->hasStarted = false;
    this->systemGivenWatcher = _systemGivenWatcher;
    this->mutex = new QMutex();
    const QString rootPathNormalized = QFileInfo(rootPath).absoluteFilePath();
    this->rootWatchedDirectory = new QString(rootPathNormalized);
}

void FullFileSystemWatcher::start() {
    SynchronizedScope synchronized(this->mutex);
    if (this->hasStarted) {
        return;
    }
    this->onCreateDirectory(true, *(this->rootWatchedDirectory));
    this->hasStarted = true;
}

FullFileSystemWatcher::~FullFileSystemWatcher() {
    free(this->mutex);
}

QByteArray FullFileSystemWatcher::calculateFileHash(const QString &filePath)
{
    QFile f(filePath);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Algorithm::Md5);
        if (hash.addData(&f)) {
            return hash.result();
        }
    }
    return QByteArray();
}

void FullFileSystemWatcher::onDirectoryEvent(const QString& directoryPath) {
    SynchronizedScope synchronized(this->mutex);
    QMap<QString, QByteArray> newlyCreatedFiles = this->getNewlyCreatedFiles(directoryPath);
    QSet<QString> newlyCreatedDirectories = this->getNewlyCreatedDirectories(directoryPath);
    QSet<QString> newlyDeletedDirectories = this->getNewlyDeletedDirectories(directoryPath);
    for (QString newFilePath : newlyCreatedFiles.keys()) {
        this->onCreateFile(newFilePath);
    }
    for (QString newDirectoryPath : newlyCreatedDirectories.toList()) {
        this->onCreateDirectory(false, newDirectoryPath);
    }
    for (QString oldDirectoryPath : newlyDeletedDirectories.toList()) {
        this->onDeleteDirectory(oldDirectoryPath);
    }
}

void FullFileSystemWatcher::onFileEvent(const QString& filePath) {
    SynchronizedScope synchronized(this->mutex);
    QFile file(filePath);

    if (file.exists()) {
        onUpdate(filePath);
    } else {
        onDelete(filePath);
    }
}

void FullFileSystemWatcher::onCreateFile(const QString& filePath) {
    QFileInfo fileInfo(filePath);
    QByteArray newHash = this->calculateFileHash(filePath);
    if (newHash.isEmpty()) {
        return;
    }
    QDir parentDir = fileInfo.absoluteDir();
    QString parentDirPath = parentDir.absolutePath();
    if (!this->directoryToTheFilesAndHashInsideMap.contains(parentDirPath)) {
        QMap<QString, QByteArray> emptyMap;
        this->directoryToTheFilesAndHashInsideMap.insert(parentDirPath, emptyMap);
    }
    this->directoryToTheFilesAndHashInsideMap[parentDirPath].insert(filePath, newHash);
    this->systemGivenWatcher->addPath(filePath);
    if (this->hasStarted) {
        emit fileCreatedSignal(filePath, newHash);
    }
}

void FullFileSystemWatcher::onCreateDirectory(const bool isInitializingRootDirectory, const QString& directoryPath) {
    if (!isInitializingRootDirectory) {
        QFileInfo fileInfo(directoryPath);
        QDir parentDir = fileInfo.absoluteDir();
        QString parentDirPath = parentDir.absolutePath();
        if (!this->directoryToTheDirectoriesInsideMap.contains(parentDirPath)) {
            QSet<QString> emptySet;
            this->directoryToTheDirectoriesInsideMap.insert(parentDirPath, emptySet);
        }
        this->directoryToTheDirectoriesInsideMap[parentDirPath].insert(directoryPath);
    }

    QDir rootPath(directoryPath);
    if (!this->directoryToTheFilesAndHashInsideMap.contains(directoryPath)) {
        QMap<QString, QByteArray> emptyMap;
        this->directoryToTheFilesAndHashInsideMap.insert(directoryPath, emptyMap);
    }

    if (!this->directoryToTheDirectoriesInsideMap.contains(directoryPath)) {
        QSet<QString> emptySet;
        this->directoryToTheDirectoriesInsideMap.insert(directoryPath, emptySet);
    }

    this->systemGivenWatcher->addPath(directoryPath);
    if (this->hasStarted) {
        emit directoryCreatedSignal(directoryPath);
    }

    QFileInfoList fileInfoList = rootPath.entryInfoList(QDir::Files | QDir::AllDirs | QDir::Hidden | QDir::NoDot | QDir::NoDotDot |
                                                        QDir::NoDotAndDotDot | QDir::NoSymLinks);
    foreach (const QFileInfo &fileInfo, fileInfoList) {
        if (fileInfo.isFile()) {
            QString absoluteFilePath = fileInfo.absoluteFilePath();
            this->onCreateFile(absoluteFilePath);
        } else if (fileInfo.isDir()) {
            QString absoluteSubdirectoryPath = fileInfo.absoluteFilePath();
            this->onCreateDirectory(false, absoluteSubdirectoryPath);
        }
    }
}

void FullFileSystemWatcher::onDeleteDirectory(const QString& directoryPath) {
    QDir rootPath(directoryPath);
    if (this->directoryToTheFilesAndHashInsideMap.contains(directoryPath)) {
        for (QString filePath : this->directoryToTheFilesAndHashInsideMap[directoryPath].keys()) {
            this->onDelete(filePath);
        }
        this->directoryToTheFilesAndHashInsideMap.remove(directoryPath);
    }

    if (!this->directoryToTheDirectoriesInsideMap.contains(directoryPath)) {
        for (QString subdirectoryPath : this->directoryToTheDirectoriesInsideMap[directoryPath].toList()) {
            this->onDeleteDirectory(subdirectoryPath);
        }
        this->directoryToTheDirectoriesInsideMap.remove(directoryPath);
    }

    QDir parentDir = QFileInfo(directoryPath).absoluteDir();
    QString parentDirPath = parentDir.absolutePath();
    if (this->directoryToTheDirectoriesInsideMap.contains(parentDirPath) &&
            this->directoryToTheDirectoriesInsideMap[parentDirPath].contains(directoryPath)) {
        this->directoryToTheDirectoriesInsideMap[parentDirPath].remove(directoryPath);
    }

    this->systemGivenWatcher->removePath(directoryPath);
    if (this->hasStarted) {
        emit directoryDeletedSignal(directoryPath);
    }
}

void FullFileSystemWatcher::onUpdate(const QString& filePath) {
    QByteArray newHash = this->calculateFileHash(filePath);
    if (newHash.isEmpty()) {
        return;
    }
    QFileInfo fileInfo(filePath);
    QDir parentDir = fileInfo.absoluteDir();
    QString parentDirPath = parentDir.absolutePath();
    if (!this->directoryToTheFilesAndHashInsideMap.contains(parentDirPath)) {
        QMap<QString, QByteArray> emptyMap;
        this->directoryToTheFilesAndHashInsideMap.insert(parentDirPath, emptyMap);
    }
    bool hashChanged = true;
    if (this->directoryToTheFilesAndHashInsideMap[parentDirPath].contains(filePath)) {
        QByteArray oldHash = this->directoryToTheFilesAndHashInsideMap[parentDirPath][filePath];
        if (oldHash == newHash) {
            hashChanged = false;
        }
    }
    if (hashChanged) {
        this->directoryToTheFilesAndHashInsideMap[parentDirPath].insert(filePath, newHash);
        if (this->hasStarted) {
            emit fileUpdatedSignal(filePath, newHash);
        }
    }
}

void FullFileSystemWatcher::onDelete(const QString& filePath) {
    QFileInfo fileInfo(filePath);
    QDir parentDir = fileInfo.absoluteDir();
    QString parentDirPath = parentDir.absolutePath();
    if (this->directoryToTheFilesAndHashInsideMap.contains(parentDirPath) &&
            this->directoryToTheFilesAndHashInsideMap[parentDirPath].contains(filePath)) {
        QByteArray hash = this->directoryToTheFilesAndHashInsideMap[parentDirPath][filePath];
        this->directoryToTheFilesAndHashInsideMap[parentDirPath].remove(filePath);
        this->systemGivenWatcher->removePath(filePath);
        if (this->hasStarted) {
            emit fileDeletedSignal(filePath, hash);
        }
    }
}

QMap<QString, QByteArray> FullFileSystemWatcher::getNewlyCreatedFiles(const QString& directoryPath) {
    QMap<QString, QByteArray> filePathsAndHashMap = this->directoryToTheFilesAndHashInsideMap[directoryPath];
    QDir dir(directoryPath);
    QMap<QString, QByteArray> output;
    QFileInfoList fileInfoList = dir.entryInfoList(QDir::Files | QDir::Hidden | QDir::NoDot | QDir::NoDotDot |
                                                   QDir::NoDotAndDotDot | QDir::NoSymLinks);
    foreach (const QFileInfo &fileInfo, fileInfoList) {
        QString absoluteFilePath = fileInfo.absoluteFilePath();
        if (!filePathsAndHashMap.contains(absoluteFilePath)) {
            QByteArray hash = this->calculateFileHash(absoluteFilePath);
            output.insert(absoluteFilePath, hash);
        }
    }
    return output;
}

QSet<QString> FullFileSystemWatcher::getNewlyCreatedDirectories(const QString& directoryPath) {
    QSet<QString> subdirectories = this->directoryToTheDirectoriesInsideMap[directoryPath];
    QDir dir(directoryPath);
    QSet<QString> output;
    QFileInfoList fileInfoList = dir.entryInfoList(QDir::AllDirs | QDir::Hidden | QDir::NoDot | QDir::NoDotDot |
                                                   QDir::NoDotAndDotDot | QDir::NoSymLinks);
    foreach (const QFileInfo &fileInfo, fileInfoList) {
        QString absoluteSubdirectoryPath = fileInfo.absoluteFilePath();
        if (!subdirectories.contains(absoluteSubdirectoryPath)) {
            output.insert(absoluteSubdirectoryPath);
        }
    }
    return output;
}

QSet<QString> FullFileSystemWatcher::getNewlyDeletedDirectories(const QString& directoryPath) {
    QSet<QString> subdirectories = this->directoryToTheDirectoriesInsideMap[directoryPath];
    QDir dir(directoryPath);
    QSet<QString> output;
    QSet<QString> currentSubdirectories;
    QFileInfoList fileInfoList = dir.entryInfoList(QDir::AllDirs | QDir::Hidden | QDir::NoDot | QDir::NoDotDot |
                                                   QDir::NoDotAndDotDot | QDir::NoSymLinks);
    foreach (const QFileInfo &fileInfo, fileInfoList) {
        currentSubdirectories.insert(fileInfo.absoluteFilePath());
    }
    for (QString subdirectory : subdirectories.toList()) {
        if (!currentSubdirectories.contains(subdirectory)) {
            output.insert(subdirectory);
        }
    }
    return output;
}
