#include "synchronizedscope.h"
#include <QDebug>

SynchronizedScope::SynchronizedScope(QMutex *_mutex)
{
    this->mutex = _mutex;
    this->mutex->lock();
}

SynchronizedScope::~SynchronizedScope()
{
    this->mutex->unlock();
}
