#ifndef MYWATCHER_H
#define MYWATCHER_H

#include <QFileSystemWatcher>
#include <QMap>
#include <QObject>
#include <QMutex>
#include <QSet>
#include "filesystemeventlistener.h"
#include <unistd.h>

class FullFileSystemWatcher : public QObject
{
    Q_OBJECT
public:
    explicit FullFileSystemWatcher(const QString &rootPath, QFileSystemWatcher *_systemGivenWatcher, QObject *parent = 0);
    void start();
    ~FullFileSystemWatcher();

signals:
    void fileCreatedSignal(QString path, QByteArray hash);
    void fileUpdatedSignal(QString path, QByteArray hash);
    void fileDeletedSignal(QString path, QByteArray hash);
    void directoryCreatedSignal(QString path);
    void directoryDeletedSignal(QString path);

public slots:
    void onDirectoryEvent(const QString& directoryPath);
    void onFileEvent(const QString& filePath);

private:
    bool hasStarted;
    QMutex *mutex;
    QFileSystemWatcher *systemGivenWatcher;
    const QString *rootWatchedDirectory;
    QMap<QString, QMap<QString, QByteArray> > directoryToTheFilesAndHashInsideMap;
    QMap<QString, QSet<QString> > directoryToTheDirectoriesInsideMap;
    QByteArray calculateFileHash(const QString &filePath);
    void onCreateDirectory(const bool isInitializingRootDirectory, const QString& directoryPath);
    void onCreateFile(const QString& filePath);
    void onUpdate(const QString& filePath);
    void onDelete(const QString& filePath);
    QMap<QString, QByteArray> getNewlyCreatedFiles(const QString& directoryPath);
    QSet<QString> getNewlyCreatedDirectories(const QString& directoryPath);
    QSet<QString> getNewlyDeletedDirectories(const QString& directoryPath);
    void onDeleteDirectory(const QString& directoryPath);
    QString transformByArrayToUserFriendlyForm(const QByteArray &arr);
};

#endif // MYWATCHER_H
